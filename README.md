# r-scripts
Everyone in the Aquatic ecology department can access this git repository.
Do not add large files or files that do not have a text format. 


## Getting started with tools
<Under construction>
The directory tools contains an R-package. Follow the following steps to use:

- Open the RStudio project in the tools directory 
- In the console tab of Rstudio type 'install()'
- In your own R studio project type 'library("rutools")'

Now you can use all the functions from the rutools package

## TO DO
## Add interpolation of HOBO loggers 22-6-2023
## Add possibility to interpolate data from other analyzers
# - Microportable
# - Ultraportable



rm(list = ls())
setwd(dirname(rstudioapi::getActiveDocumentContext()$path)) # set working directory to location where script is located
getwd() # check if working directory is set correct

library(tidyverse)
library(plotly)
library(data.table)
library(readr)
library(readxl)
library(stringr)

################################################################################
## Read multiplexer files
################################################################################

#####################################
### READ IN FILES
acs <- list.files(path = "./data/acs", pattern = ".csv", full.names = T)

for (i in seq_along(acs)) {
  
  assign(paste("acs.df", i, sep = ""), fread(acs[i], showProgress = T, verbose = T, fill = T))
  
}

hobo <- list.files(path = "./data/hobo", pattern = ".xlsx", full.names = T)

## CREATE DATAFRAMES
combined_df <- data.frame()

for (i in seq_along(hobo)) {
  file_name <- tools::file_path_sans_ext(basename(hobo[i]))
  name <- str_extract(file_name, "(A[1-4]|B[1-4]|C[1-4]|D[1-4])")
  type <- str_extract(file_name, "(air|soil)")
  temp_df <- read_xlsx(hobo[i], skip = 1, na = c("NA","NaN","Invalid Number"), col_names = TRUE, col_types = c("date", "numeric", rep("skip", ncol(read_xlsx(hobo[i])) - 2)))
  
  # Rename column names
  colnames(temp_df) <- c("dt", "temp")
  
  # Add a new columns with the extracted strings and edit timezone
  temp_df$Chamber_ID <- name
  temp_df$air_soil <- type
  tz(temp_df$dt) <- "Europe/Amsterdam"
  
  # Append the data to the combined dataframe
  combined_df <- na.omit(rbind(combined_df, temp_df))
}


Multiplexer <- mget(ls(pattern="^acs\\.")) %>%
  bind_rows(.id = "Source") %>%
  mutate(dt = as.POSIXct(paste(Date,Time), format = "%Y-%m-%d %H:%M:%OS", tz = "Europe/Amsterdam")) %>%
  relocate(dt, .after = Time) %>%
  as.data.frame()

Multiplexer <- Multiplexer %>%  ### remove ..."colnumber" from columns with identical names
  setNames(gsub("\\.\\.\\.\\d{2}", "", colnames(.)))

################################################################################
## Read and interpolate `LICOR-7810` data
################################################################################

`LICOR-7810` <- read.table("TG10-01077-2023-05-10T080000.data", header = TRUE, sep = "\t", skip = 5)

`LICOR-7810`$dt <- as.POSIXct(paste(`LICOR-7810`$DATE, `LICOR-7810`$TIME), format = "%Y-%m-%d %H:%M:%S", tz = "Europe/Amsterdam")

`LICOR-7810` <- `LICOR-7810`[-1, ] #%>% filter(DATE == "2023-05-10")

# Convert dt column to POSIXct format
`LICOR-7810`$dt <- as.POSIXct(`LICOR-7810`$dt)



# Interpolate the CH4 values using the approx() function
Multiplexer$`CH4_dry (ppm)` <- approx(`LICOR-7810`$dt, `LICOR-7810`$CH4, Multiplexer$dt, method = "linear")$y/1000 ## Devide by 1000 to go from ppb to ppm
Multiplexer$`CO2_dry (ppm)` <- approx(`LICOR-7810`$dt, `LICOR-7810`$CO2, Multiplexer$dt, method = "linear")$y ## Already in ppm
Multiplexer$`H2O (ppm)` <- approx(`LICOR-7810`$dt, `LICOR-7810`$H2O, Multiplexer$dt, method = "linear")$y ## Already in ppm
Multiplexer$Cavity_P_kPa <- approx(`LICOR-7810`$dt, `LICOR-7810`$CAVITY_P, Multiplexer$dt, method = "linear")$y
Multiplexer$Cavity_T_C <- approx(`LICOR-7810`$dt, `LICOR-7810`$CAVITY_T, Multiplexer$dt, method = "linear")$y
Multiplexer$Thermal_enclosure_T <- approx(`LICOR-7810`$dt, `LICOR-7810`$THERMAL_ENCLOSURE_T, Multiplexer$dt, method = "linear")$y
Multiplexer$`Input voltage (V)` <- approx(`LICOR-7810`$dt, `LICOR-7810`$INPUT_VOLTAGE, Multiplexer$dt, method = "linear")$y

# calculate average values because only 2 loggers were used for 4 chambers. This should be improved so all values are used when each chamber has its own logger. 
hobo_air <- combined_df %>%
  filter(air_soil=='air', Chamber_ID %in% Multiplexer$Chamber_ID) %>%
  group_by(dt = as.POSIXct(floor_date(dt, "minute"))) %>% # values are rounded off to 1 minute because measure interval is 1 minute but loggers are not logging at exact same time.
  summarise(Chamber_ID2 = paste(Chamber_ID, collapse = ","), # this column shows the loggers that were used to average
            mean_temp = mean(temp))


hobo_soil <-  combined_df %>%
  filter(air_soil=='soil', Chamber_ID %in% Multiplexer$Chamber_ID) %>%
  group_by(dt = as.POSIXct(floor_date(dt, "minute"))) %>%
  summarise(Chamber_ID2 = paste(Chamber_ID, collapse = ","),
            mean_temp = mean(temp))


Multiplexer$T_air <- approx(hobo_air$dt, hobo_air$mean_temp, Multiplexer$dt, method = "linear")$y
Multiplexer$T_soil <- approx(hobo_soil$dt, hobo_soil$mean_temp, Multiplexer$dt, method = "linear")$y



#### save csv file in new folder ####

# Specify the folder name and CSV file path
folder_name <- "interpolated_data"
csv_file <- file.path(folder_name, "interpolated_data.csv")


# Create the folder if it doesn't exist
if (!dir.exists(folder_name)) dir.create(folder_name)


# Convert numeric columns to character columns with dot as decimal separator
Multiplexer[] <- lapply(Multiplexer, function(x) {
  if (is.numeric(x))
    format(x, decimal.mark = ".")
  else
    x
})

# Write the data to the CSV file using the 'fwrite' function from 'data.table'
write.csv2(Multiplexer, csv_file, quote = FALSE)

# fileEncoding="Windows-1252"


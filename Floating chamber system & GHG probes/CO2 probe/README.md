# Relative to absolute CO₂ concentration conversion

The RU CO₂ concentration probe measures CO₂ concentration in the soil, and to a maximum depth of 3 meters in water. Measurements of CO₂ in the water column are influenced by the combined atmospheric and water pressure (hydrostatic pressure) and hence, require corrections. In addition, additional corrections are required to convert relative (e.g. ppm) to non-relative dissolved gas concentrations (e.g. mg/L). 

This script provides the calculations necessary to apply the corrections on the aqueous CO₂ measurements collected with the RU CO₂ probe.


## Thermodynamic solubility constant

The thermodynamic solubility constant (K0) used to calculate aqueous concentrations from headspace concentrations is based on the empirical data of [Harned & Davis, 1943](https://pubs.acs.org/doi/pdf/10.1021/ja01250a059). I refitted Equation 8 of Harned & Davis on a subset of their data (until 40 °C) to achieve the best fit (and hence most precise predicted values) for the water temperature range that we will measure in. The related code and data can be found in sub folder 'Calculation of solubility constant'. The resulting parameters of the fit are included in the main script.


```bash
pK0 = -(2450.65 / (WaterTemp+273.15)) + 14.46495 - 0.01603376 * (WaterTemp + 273.15)
K0 = 10**-pK0   # thermodynamic solubility constant (molality of CO2 / PCO2 in atm)

```


## Usage

Replace the source data ('dummy_data.csv') with your own file (using similar column names) and edit the following variables in the script to match the measuring conditions:

```bash
#---------------------- USER INPUT --------------------#

plaatsnaam = "Aldeboarn"        # wordt gebruikt om valversnelling (nodig voor hydrostatische druk) ter hoogte van meetlocatie op te halen en luchtdruk van dichtstbijzijnde weerstation
hoogte_water_kolom_boven_probe_head = 0.3   # in meters; voor berekening hydrostatische druk

#-------------------- END OF USER INPUT ------------------#
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[MIT](https://choosealicense.com/licenses/mit/)